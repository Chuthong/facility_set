# joystick teleop

### 1. create catkin workspace
```bash=
# goto destination file location
mkdir -p catkin_ws/src
cd catkin_ws
catkin_make
```
### 2. create catkin package
```bash=
# Still stay destination file location
cd src
catkin_create_pkg joystick_teleop rospy roscpp std_msgs
```
### 3. copy file
- **copy** " joystick.cpp ", " joystick.h ", " joystick_teleop.cpp " --> paste to
destination folder/catkin_ws/src/joystick_teleop/src 
- **copy** CMakelists.txt and Repaste to 
destination folder/catkin_ws/src/joystick_teleop/CMakelists.txt

### 4. catkin make
```bash=
# goto destination file location
cd catkin_ws
catkin_make
```
