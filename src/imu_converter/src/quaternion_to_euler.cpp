#include "ros/ros.h"
#include "std_msgs/String.h"
#include "std_msgs/Float64MultiArray.h"
#include "geometry_msgs/Vector3.h"
#include "sensor_msgs/Imu.h"
#include <fstream>
// #include <sstream>
// #include <iostream>
#include <unistd.h>
#include "stdio.h"
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <ctime>
#include "time.h"
#include <chrono>
// #include "math.h"

#define QUATERNION_TOPIC    "/seg0/imu/data"
#define EULER_TOPIC         "/seg0/imu/data_euler"
#define EULER_DEG_TOPIC     "/seg0/imu/data_euler_deg"


//*******************************************************//
//                         config                        //
//*******************************************************//
#define loop_rate_Hz        50
#define open_debug_mode     false    //true or false
#define save_to_csv         false   //true or false
std::string file_name = "smallNonPaintPipePlasticCap_012.csv";
std::string file_detail = "Friction of i-crawl foot experiment";
std::string location_file = /*from catkin_ws*/ "friction_experiment/" + file_name;

//********************************************************//
//********************************************************//


float qx,qy,qz,qw;

// void callback(const geometry_msgs::Quaternion::ConstPtr& msg)
void callback(const sensor_msgs::Imu::ConstPtr& msg)
{
//   ROS_INFO("I heard: [%s]", msg->data.c_str());
    qx = msg->orientation.x;
    qy = msg->orientation.y;
    qz = msg->orientation.z;
    qw = msg->orientation.w;
}

float rad2deg(float rad){
    return rad*180/M_PI;
}




// Save data to CSV
std::fstream fout;


int main(int argc, char **argv){


 std::cout      << "                 (   )\n"
                << "               (       )\n"
                << "             (    ROS    )\n"
                << "               (       )\n"
                << "                 (___)\n"
                << "\n"
                << "                  |  A       \n"
                << "          ________|  |________ \n"
                << "         |                    |\n"
                << "         |                    |\n"
                << "  sensor_msgs::Imu   geometry_msgs::Vector3 \n"
                << "     /imu/data         /seg0/imu/data_euler\n"
                << "         |                    |\n"    
                << "         |                    |\n"
                << "         V                    |\n"
                << "     ____________        ____________ \n"
                << "    |            |      |            |\n"
                << "    |            |      |            |\n"
                << "    | QUATERNION | >>>> |    EULER   |\n"
                << "    |            |      |            |\n"
                << "    |____________|      |____________|"
                <<"  develop by C.thirawat \n"
                <<"\n";

                                                



    ros::init(argc, argv, "quaternion_to_euler");
    ros::NodeHandle nh;
    ros::Publisher euler_pub = nh.advertise<geometry_msgs::Vector3>(EULER_TOPIC, 1000);
    ros::Publisher euler_deg_pub = nh.advertise<geometry_msgs::Vector3>(EULER_DEG_TOPIC, 1000);
    ros::Subscriber quat_sub = nh.subscribe(QUATERNION_TOPIC, 1000, callback);
    ros::Rate loop_rate(loop_rate_Hz);

    geometry_msgs::Vector3 euler_msg;
    geometry_msgs::Vector3 euler_deg_msg;


    //Initial file CSV
    // std::string file_name = "test001_joe.csv";
    // std::string location_name = "friction_experiment/" + file_name;
    if(save_to_csv){
        fout.open(location_file,std::ios::out | std::ios::app);
        fout    << file_name << "\n"
                << file_detail
                << "time(sec)" <<"," << "roll" << "," << "pitch" << "," << "yaw" << "\n";
    }

    //Initial time
    auto begin = std::chrono::high_resolution_clock::now();    

    while (ros::ok()){
        

        // roll (x-axis rotation)
        double sinr_cosp = 2 * (qw * qx + qy * qz);
        double cosr_cosp = 1 - 2 * (qx * qx + qy * qy);
        euler_msg.x = atan2(sinr_cosp, cosr_cosp);
        euler_deg_msg.x = rad2deg(euler_msg.x);

        // pitch (y-axis rotation)
        double sinp = 2 * (qw * qy - qz * qx);
        if (abs(sinp) >= 1){
            euler_msg.y = copysign(M_PI / 2, sinp); // use 90 degrees if out of range
        }
        else{
            euler_msg.y = asin(sinp);
        }
        euler_deg_msg.y = rad2deg(euler_msg.y);

        // yaw (z-axis rotation)
        double siny_cosp = 2 * (qw * qz + qx * qy);
        double cosy_cosp = 1 - 2 * (qy * qy + qz * qz);
        euler_msg.z = atan2(siny_cosp, cosy_cosp);
        euler_deg_msg.z = rad2deg(euler_msg.z);
        
        
        //Publish
        euler_pub.publish(euler_msg);
        euler_deg_pub.publish(euler_deg_msg);

        //Time calculation
        auto end = std::chrono::high_resolution_clock::now();
        auto dur = end - begin;
        double time_s = std::chrono::duration_cast<std::chrono::microseconds>(dur).count()/1000000.0;
        
        //Save data to CSV
        if(save_to_csv){
        fout    << time_s          << ","
                << euler_deg_msg.x << ","
                << euler_deg_msg.y << ","
                << euler_deg_msg.z << "\n";
        }
        if(open_debug_mode){
            printf("time %f sec |  %f , %f , %f \n", time_s ,euler_msg.x*180/M_PI,euler_msg.y*180/M_PI,euler_msg.z*180/M_PI);
        }

        ros::spinOnce();
        loop_rate.sleep();
    }//while
    
}//main


// do something
