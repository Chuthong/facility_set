# __ROSserial_arduino (for object detection)__
This project aims to detect object by using IR sensor which connect to arduino board
for [READ Markdown file](https://gitlab.com/Chuthong/facility_set/-/blob/master/Document/rosserial_arduino/readme.md)

# Hardware setup
To get started, connect your ranger to your Arduino as shown below. Make sure to connect the signal pin of the ranger to analog input 0.

![Arduino_with_IR](arduino_ir_ranger.jpg ) <br />

__Fig.1__ Hardware connection between Arduino UNO and IR range sensor. The output signal from the sensor (yellow wire) connect to analog input 0 of arduno board. The Vcc(red wire) and Gnd(black wire) of the sensor connect to 5V and Gnd pin of arduino board respectively.

<br />

### ** If you don't have IR range sensor, you can use potentiometer instead of IR range sensor. because the output of potentiometer is also analog.

![arduino_poten](arduino_poten.png)

__Fig.2__ Hardware connection between Arduino UNO and Potentiometer (i.e., variable resistor). The output signal from the sensor (yellow wire) connect to analog input 0 of arduno board. The Vcc(red wire) and Gnd(black wire) of the sensor connect to 5V and Gnd pin of arduino board respectively.
_____________________
<br />

# Software setup

## 1. Install Arduino IDE and Robot Operating System(ROS)
Before start this project you need to have these OS and softwre first.
- This is a [Arduino_software](https://www.arduino.cc/en/software) link.
- This is a [ROS_installation](http://wiki.ros.org/ROS/Installation) link.


## 2. Install libraries
- ROS-serial on Ubuntu
    1. Open_terminal
    ( **Ctrl + Alt + T** or **search "terminal" on the conputer**).
    2. At the terminal type this respectively (see __"Note"__ first)
        - **Note:** Replace **indigo** with the name of the release you're installing from(e.g., kinetic, noatic, hydro).
        
        <br />
        <br />

        ```bash
        sudo apt-get install ros-indigo-rosserial-arduino
        sudo apt-get install ros-indigo-rosserial  
        ```

- ROS on Arduino IDE
    1. Open Arduino IDE (search __"Arduino IDE"__)
    
    2. See the left-top of the window. <br />
    Goto → sketch → Include libralies → Manage libralies. <br />
    Then search __"rosserial"__ and __PLEASE USE Version 0.7.9__ and install. <be />
    ![rosserial_libralies_manager](arduino_Library_Manager.png)
    __Fig.3__ Rosserial arduino library Installing by Michael Ferguson on library manager
    
    3. __(option)__ To check that, Are your arduino IDE and ROS libralies work ? <br />
    Goto → File → Example → Rosserial arduino libraries → HelloWorld(Recommend)  After that, burn this code to Arduino board.<br />
    __Note1:__ On the __top-left bar in Arduino IDE__ Goto → Tool → [select]
        - **Board** →→ "__Arduino Uno__" || for this project we use __Uno__, select the board type that you are using.
        - **Processer** →→ "__ATmega 320p (Old Bootloader)__"
        - **Port** →→ "__/dev/ttyUSB*__" (e.g., 0 and 1)
        <br />
        <br />
        <br />
        ![Hello_world](HelloWorld_ROS.png) 
        <br />
    __Fig.4__ Checking ROS libraries at Arduino IDE by going File → Example → Rosserial arduino libraries → HelloWorld

    __Note2:__ If you **cannot** upload the code to arduino board because it shows
    __"Can't open device: '/dev/ttyUSB0' Permission denied"__ like this
        ![permission_denied](permission_denied.png)
        __Fig.5__ Permission denied

    Please follow below to solve this problem.
    - Open new terminal
    - check your arduino USB port
        ```bash
        ls /dev/ttyUSB*
        ```
        it will show your arduino port (e.g., /dev/ttyUSB0)
        <br />
        <br />
    - allow port permission (If it shows "/dev/ttyUSB0")
        ```bash
        sudo chmod 777 /dev/ttyUSB0
        ```

## 3. __`The arduino code`__ for receive IR sensor data
- Open arduino IDE and __Goto → New → File__ then copy in the code below
    ```c++
    // Use the following line if you have a Leonardo or MKR1000
    //#define USE_USBCON

    // In this project we use Arduino nano board

    // include ROS and used massages
    #include <ros.h>
    #include <std_msgs/Bool.h>
    #include <std_msgs/Float32.h>

    // define topic name
    #define DETECT_TOPIC            "/IR_detect_topic"
    #define IR_TOPIC                "/IR_value_topic"

    // define input and output pin
    #define internal_LED_pin        LED_BUILTIN
    #define outernal_LED_pin        2
    #define IR_pin                  A0

    //**************************parameter**************************//
    ros::NodeHandle nh;
    std_msgs::Bool detect_msg;
    std_msgs::Float32 ir_msg;

    bool ir_detect = false;
    double ir_value = 0.0;
    double ir_old_value = 0.0;

    //**************************function**************************//
    double get_IR(uint16_t value);

    //*******************ROS PUB and SUB initiation***************//
    ros::Publisher IR_DETECT_PUB(DETECT_TOPIC ,&detect_msg);
    ros::Publisher IR_VALUE_PUB(IR_TOPIC ,&ir_msg);

    //*************************************************************//
    //                           VOID SETUP                        //
    //*************************************************************//
    void setup(){
    //initial Node, Pubblisher ,and Subscriber
    nh.getHardware()->setBaud(57600);
    nh.initNode();
    nh.advertise(IR_DETECT_PUB);
    nh.advertise(IR_VALUE_PUB);
    
    pinMode(internal_LED_pin,OUTPUT);
    pinMode(outernal_LED_pin,OUTPUT);
    }


    //*************************************************************//
    //                             VOID LOOP                       //
    //*************************************************************//

    void loop(){
        // Read analog from Pin A0
        uint16_t analog = analogRead(IR_pin);

        //******************************//
        //         select sensor        //
        //******************************//
        //------> for IR sensor <------ //
        ir_value = get_IR(analog);      //
                                        //
        //----> for Potentiometer <---- //
        // ir_value = analog;           //
                                        //
        //******************************//

        //signal filter
        ir_value = ir_value*0.3 + ir_old_value*0.7;

        //If distance between sensor and object <= 12 cm
        // output is ture, unless output is false
        if(abs(ir_value) <= 12.0){
            ir_detect = true;
            // toggle the LEDs to HIGH
            digitalWrite(internal_LED_pin, HIGH); 
            digitalWrite(outernal_LED_pin, HIGH);
        }
        else{
            ir_detect = false;
            // toggle the LEDs to LOW
            digitalWrite(internal_LED_pin, LOW);
            digitalWrite(outernal_LED_pin, LOW);
        }

        ir_old_value = ir_value;

        //add the data to massage
        detect_msg.data = ir_detect;
        ir_msg.data = ir_value;

        // Publish data message to ROS
        IR_DETECT_PUB.publish( &detect_msg);
        IR_VALUE_PUB.publish( &ir_msg);
        
        nh.spinOnce();
    }//void loop



    //*************************************************************//
    //                             FUNCTION                        //
    //*************************************************************//

    //return distance (cm)
    double get_IR(uint16_t value){
        if (value < 16) value = 16;
        return 2076.0 / (value - 11.00);
    }
    ```
    The code description is descriped in the code, you read the detail of code via comment line
__________

# __Launching the App__
## - Before run this system, you have to done with these.
- Ubuntu OS
- ROS
- ROS-serial libraries on Ubuntu
- Arduino IDE
- ROS-serial libraries on Arduino IDE
- Burn the code __( "Software setup → 3.The arduino code" )__ to arduino board
### If you have done it all, you can go to the next step.

## 1. Run __"roscore"__ on a termianl
- Open terminal and type 
    ```bash
    roscore
    ```
## 2. Run __"serial_node.py"__ to communicate with Arduino node.
- Open another terminal and type
    ```bash
    rosrun rosserial_python serial_node.py _baud:=57600 
    ```
- (__Optional__) Example for Change Serial USB port or Baudrate
    ```bash
    rosrun rosserial_python serial_node.py _port:=/dev/ttyUSB0 _baud:=115200 
    ```
## 3. To check that Is your systems are communicating.
- Open another termainal and type this
    ```bash
    rostopic list
    ```
    you will see output like this
    ![rostopic_list](rostopic_list_output.png)<br />
    __Fig.6__ This figure shows the topic which are working. <br /><br />

- Then __Echo the data__ from arduino board and see the sensory feedback data.
    ```bash
    rostopic echo /IR_value_topic
    ```
    you could see the data like
    ![rostopic_echo_value](rostopic_echo_output_value.png)<br />
    __Fig.7__ This figure shows the value (cm.) which is published from arduino board to __"/IR_value_topic"__. <br /><br /><br /><br />

- Another one is detection data. It will send __"true"__ when object is closer (value is less than or equre 20 cm) and send __"false"__ when object is too far (value is more than 20 cm). To __Echo the data__ from arduino board ans see the detection data type this below to a terminal.
    ```bash
    rostopic echo /IR_detect_topic
    ```
    you could see the data like
    ![rostopic_echo_detect](rostopic_echo_output_detect.png)<br />
    __Fig.8__ This figure shows the detection value as boolean type (true or false) which is published from arduino board to __"/IR_detect_topic"__. It will send __"true"__ when object is closer (value is less than or equre 20 cm) and send __"false"__ when object is too far (value is more than 20 cm). <br /><br />


# __Reference__
- [rosserial_arduino](http://wiki.ros.org/rosserial_arduino/Tutorials)
- [std_msgs](http://wiki.ros.org/std_msgs)

<br /><br /><br />
Developed by Thirawat Chuthong
