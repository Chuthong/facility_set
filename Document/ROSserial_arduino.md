**ROS serial with Arduino board**
=============================
This is How to installation ROS serial and example code.  

>## This is what you need before you get started
>1. Arduino board.
>2. Arduino IDE (This is the easiest way to install rosserial libraries and edit).
>3.  ROS(Robot operating system) in your computer.


# Install Libraries
## 1. ROS-Serial on ROS
1. Open terminal 
2. At the terminal type (Don't forget to change __indigo__ to __kinetic__ if your ROS are)
    ```code
    sudo apt-get install ros-indigo-rosserial-arduino
    sudo apt-get install ros-indigo-rosserial  
    ```
    now your install ROS Serial on your ROS but not on your Arduino IDE

## 2. ROS on Arduino IDE
1. Open Arduino IDE
2. Goto → sketch → Include libraries → Manage libralies.  
    then search __"rosserial"__ and __install__ \n
    ![IDE](figure/arduino_Library_Manager.png)
3. Goto → File → Example → Rosserial arduino libraries → HelloWorld(Recommend)  After that, __burn this code to Arduino board__  


# 3. Using all system
1. Run "roscore"  
    Open terminal
    ```code
    roscore
    ```


2. Run "serial_node.py" to communicate with Arduino node.  
    Open terminal
    ```code
    rosrun rosserial_python serial_node.py
    ```

    (Option) Example for Change __Serial USB port__ or __Baudrate__
    ```code
    rosrun rosserial_python serial_node.py _port:=/dev/ttyUSB0 _baud:=115200 
    ```


3. To check Are this system complete.  
    Open terminal
    ```code 
    rostopic list
    rostopic echo /chatter
    ```

Congratuation, You can do it.
=====
For this below, I provide the example code that can help you to extend your ability.  
-----  
#
# Publish Int16MultiArray
```c++
#include <ros.h>
#include <std_msgs/Int16MultiArray.h>

#define array_lengh 8

ros::NodeHandle nh;

std_msgs::Int16MultiArray array_msg;

ros::Publisher array_pub("array_topic" ,&array_msg);

// initial array
int array[array_lengh] = {0,0,0,0,0,0,0,0};

void setup()
{
 	nh.initNode();
	nh.advertise(array_pub);
	array_msg.data_length = array_lengh; //set messege has 8 data
  	array_msg.data = (int *)malloc(sizeof(int)*array_lengh);
}

void loop()
{

//Give I is data you want push to ROS
  for(int i = 0; i < 8; i++){
    array_msg.data[i] = i ;
    
  }

// Publish to ROS
array_pub.publish( &array_msg);
nh.spinOnce();
```
Output data is  
data = [0,1,2,3,4,5,6,7]
_____

# Subscribe Int16MultiArray

```c++
#include <ros.h>
#include <std_msgs/String.h>
#include <std_msgs/Int16MultiArray.h>

#define magnet_array_lengh 2
#define MAGNET_TOPIC  "magnet"
#define CHAT_TOPIC    "chatter"
#define ARRAY_TOPIC   "array"


//************parameter***************//

nh.getHardware()→setBaud(115200);  // --- >  IF YOU WANT TO CHANGE BAUDRATE
						// 57600 is default baudrate, if you dont have this line

ros::NodeHandle nh;
std_msgs::Int16MultiArray magnet_msg;
std_msgs::Int16MultiArray array_msg;
std_msgs::String chat_msg;

int magnet_cmd[magnet_array_lengh] = {1,1};


//*************FUNCTION***************//

void magnetCallback( const std_msgs::Int16MultiArray &msg);

//************************ROS PUB and SUB initiation*********************//
ros::Publisher CHT_PUB(CHAT_TOPIC ,&chat_msg);
ros::Publisher ARR_PUB(ARRAY_TOPIC ,&array_msg);
ros::Subscriber<std_msgs::Int16MultiArray> MAG_SUB(MAGNET_TOPIC, &magnetCallback );


//***************************************//
//               VOID SETUP              //
//***************************************//
void setup()
{
  //initial Node, Pubblisher ,and Subscriber
  nh.initNode();
  nh.advertise(CHT_PUB);
  nh.subscribe(MAG_SUB);
  nh.advertise(ARR_PUB);
  
//  array_msg.data_length = magnet_array_lengh; //set messege has 8 data
//  array_msg.data = (int *)malloc(sizeof(int)*magnet_array_lengh);
//  array_msg.data = magnet_cmd;
  magnet_msg.data_length = magnet_array_lengh;
  magnet_msg.data = (int *)malloc(sizeof(int)*magnet_array_lengh);
  array_msg.data_length = magnet_array_lengh;
  array_msg.data = (int *)malloc(sizeof(int)*magnet_array_lengh);

  pinMode(2,OUTPUT);
  pinMode(3,OUTPUT);
  
}



//*************************************//
//              VOID LOOP              //
//*************************************//

void loop()
{
  
  array_msg.data = magnet_msg.data;
  digitalWrite(2,magnet_cmd[0]);
  digitalWrite(3,magnet_cmd[1]);

  
  // Publish data message to ROS
  ARR_PUB.publish( &array_msg);
  CHT_PUB.publish( &chat_msg);
  nh.spinOnce();
}//void loop


//***********************************//
//               FUNCTION            //
//***********************************//

void magnetCallback( const std_msgs::Int16MultiArray &msg){
  // you need to find message parameter that you initial data_lentgh 
  // and allocate memory to receive data from message
  magnet_msg.data = msg.data;

  //get data from the message to array
  for(int i = 0; i < 2; i++){
    magnet_cmd[i] = magnet_msg.data[i];
  }
}
```


Developed by Joe Thirawat Chuthong
