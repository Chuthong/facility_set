# Stick insect from gorobots
This manual aims to create new seperate workspace for stick insect simulation.
It doesn't care about another project, we just copy and paste then said good bye to gorobots :smile: 
These lead you to be simple ROS system 

### Simulation require
- Ubuntu 16.04 or later
- ROS(Robot Operating System)
- CoppeliaSim with ROS interface
- Clone [gorobot](https://gitlab.com/ens_sdu/gorobots) from Gitlab

**Note**: In gorobots has many projects. So, you have to copy some file which relate to medaextra project

### Initial variable
 To represent folder and file which use to explain
 from this section you will see 3 parts : A, B, and C
- gorobot > Projects > medaextra : **This folder refered to A**
- gorobot > controller > medaextra > locomotion_control : **All  file in this folder refered to B**
- gorobot > utils > v-rep_simulations > medaextra : **All file in this foder refered to C**

### Let's get started
 - Paste **A** to somewhere (You may paste A folder to Destop)
 - Then Goto **A** > catkin_ws > src > medaextra_controller : create folder name "**src**"
 - Paste B on this "**src folder**"
 - Goto A : paste C in this folder
    In folder A should be like this

    ![](https://i.imgur.com/Li9MoUl.png)

_________


### CoppeliaSim
**Before open coppeliaSim you must "roscore" fisrt
To run roscore -- open terminal
```bash=
roscore
```
To open CoppeliaSim -- open termianl
```bash=
cd ~/CoppeliaSim_Edu_V4_2_0_Ubuntu20_04/
./coppeliaSim.sh
```

After open coppeliaSim
1. Open **medaextra.ttt** on CoppeliaSim -- It should be in folder A which you pasted


2. Double click **script icon :page_facing_up:** on abdomen

![script](https://i.imgur.com/q1HUyM0.png)

It should show a windown that contain path to lua file


3. In script file, you need to edit path of **lua file**
in this manual, it should be in folder A

![](https://i.imgur.com/hVmoItV.png)


**Note**: The path will start at file.ttt location : if your lua file is stay at another place, you can find that path by start path location at file.ttt.
**For example**
- set path at script file in CoppeliaSim program
```lua=
package.path = package.path .. (';' .. sim.getStringParam(sim.stringparam_scene_path) .. '/?.lua')
require 'medaextra'
```
This path is short because file.ttt and lua file are the same place


4. Set Cmakelist.txt
Goto folder A > catkin_ws > src > medaextra > **Cmakelist.txt**
copy code below and replace at **Cmakelist.txt**
```cmake=
#cmake_minimum_required(VERSION 3.0.2)
cmake_minimum_required(VERSION 2.8.3)
project(medaextra_controller)

## Compile as C++11, supported in ROS Kinetic and newer
add_compile_options(-std=c++11)

## Find catkin macros and libraries
## if COMPONENTS list like find_package(catkin REQUIRED COMPONENTS xyz)
## is used, also find other catkin packages

set(EXECUTABLE_OUTPUT_PATH ${PROJECT_SOURCE_DIR})
set(LIBRARY_OUTPUT_PATH ${PROJECT_SOURCE_DIR})

message("===============================")
message(${PROJECT_SOURCE_DIR})
message("===============================")

find_package(catkin REQUIRED COMPONENTS
  geometry_msgs
  roscpp
  rospy
  std_msgs
)

include_directories(
# include
  ${catkin_INCLUDE_DIRS}
)

add_executable(medaextra_controller
    src/adaptive_controller
    src/adaptiveSF
    src/controller
    src/duallearner
    src/rbfn
    src/so2CPG
    src/modularNeuralController
)

add_dependencies(medaextra_controller ${catkin_EXPORTED_TARGETS})
target_link_libraries(medaextra_controller ${catkin_LIBRARIES})

#____________________________________________________________

message("
⠀⠀⠀⠀⣠⣶⡾⠏⠉⠙⠳⢦⡀⠀⠀⠀⢠⠞⠉⠙⠲⡀⠀
⠀⠀⠀⣴⠿⠏⠀⠀⠀⠀⠀⠀⢳⡀⠀⠀⡏⠀⠀⠀⠀⠀⢷
⠀⠀⢠⣟⣋⡀⢀⣀⣀⡀⠀⣀⡀⣧⠀⢸⠀⠀⠀⠀⠀  ⡇
⠀⠀⢸⣯⡭⠁⠸⣛⣟⠆⡴⣻⡲⣿⠀⣸⠀meda⠀⠀⡇
⠀⠀⣟⣿⡭⠀⠀⠀⠀⠀⢱⠀⠀⣿⠀⢹⠀extra⠀⡇
⠀⠀⠙⢿⣯⠄⠀⠀⠀⢀⡀⠀⠀⡿⠀⠀⡇⠀⠀⠀  ⡼
⠀⠀⠀⠀⠹⣶⠆⠀⠀⠀⠀⠀⡴⠃⠀⠀⠘⠤⣄⣠⠞⠀
⠀⠀⠀⠀⠀⢸⣷⡦⢤⡤⢤⣞⣁⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀
⠀⠀⢀⣤⣴⣿⣏⠁⠀⠀⠸⣏⢯⣷⣖⣦⡀⠀⠀⠀⠀⠀⠀
⢀⣾⣽⣿⣿⣿⣿⠛⢲⣶⣾⢉⡷⣿⣿⠵⣿⠀⠀⠀⠀⠀⠀
⣼⣿⠍⠉⣿⡭⠉⠙⢺⣇⣼⡏⠀⠀⠀⣄⢸⠀⠀⠀⠀⠀⠀
⣿⣿⣧⣀⣿.........⣀⣰⣏⣘⣆⣀⠀

")
```
Save and catkin_make in terminal at catkin_ws folder
From this process the program should generate **executable file** which is at " catkin_ws > src > medaextra > *medaextra_controller* "

5. Then, we need to change the path of **executable file** in **lua file**
- Open lua file (in folder A)
- See Line 219
set path result to point to **executable file** of Node
```lua=219
result=sim.launchExecutable( sim.getStringParameter(sim.stringparam_scene_path) .. '/catkin_ws/src/medaextra_controller/'..rosnode[i], '/cpg_topic', 0)
```

then check Line 216 which is name of executable file. Plz change to the same
```lua=216
local rosnode = {'medaextra_controller'}
```
**__executable__ file is bin file that was generated from catkin_make. you may try to find by starting from *build folder*
** Build folder will be generated from $ catkin_make in catkin_ws



**For example** my executable file is here

    ![](https://i.imgur.com/3pAkl77.png)
    
that mean my path of executable file is "/catkin_ws/src/medaextra_test/"
```lua=219
result=sim.launchExecutable( sim.getStringParameter(sim.stringparam_scene_path) .. '/catkin_ws/src/medaextra_test/'..rosnode[i], '/cpg_topic', 0)
```


6. Change path to **rbfn_files** in rbfn.h
Goto catkin_ws > src > medaextra > src > rbfn.h

To set the **rbfn_files folder** path
see line 50 in rbfn.h
__For example__

    ![](https://i.imgur.com/pmOSPnE.png)

Then I set my path to "/home/thirawat/workspace/medaextra/catkin_ws/src/medaextra_controller/src/"
```cpp=50
string file_directory = "/home/thirawat/workspace/medaextra_test/catkin_ws/src/medaextra_test/src/";
```


To check that is ok --> Directly run this node ** Don't forget to roscore!! and source setup.bash of this project

```bash
rosrun medaextra medaextra_controller
```
if it cannot run it will show you the current path --> if it incorrect plz edit

7. Check Topic list --> it may be 2 types of topic because of version of simulation(i.e., V-rep, CoppeliaSim)
Then, Change **ros topic name** from "**vrep_ros_interface**" to "**sim_ros_interface**"

Show in terminal by : rostopic list

![](https://i.imgur.com/lqrtzme.png)


Goto adaptive_controller.cpp

![](https://i.imgur.com/fOhZggD.png)

Then change "**vrep_ros_interface**" to "**sim_ros_interface**"

Check package.xml --> tag name should be "package name" same as package name in CMakelist.txt
_____

If everything is ok, you can run this controller by clicking play icon in CoppeliaSim. Then, you will see the stick insect walk -- enjoy :smile: 

____

### Now Controller code
```c=
#include "ros/ros.h"
#include "ros/console.h"
#include "std_msgs/String.h"
#include <visualization_msgs/Marker.h>

#include <sstream>
#include <sys/ioctl.h>
#include <termios.h>
 #include <cmath>

 #include <iostream>
#include <fstream>
 #include <iomanip>
 
#include "adaptive_controller.h"


bool kbhit()							//used for checking for terminal input, without pausing the loop
{
    termios term;
    tcgetattr(0, &term);

    termios term2 = term;
    term2.c_lflag &= ~ICANON;
    tcsetattr(0, TCSANOW, &term2);

    int byteswaiting;
    ioctl(0, FIONREAD, &byteswaiting);

    tcsetattr(0, TCSANOW, &term);

    return byteswaiting > 0;
}


int getch()
{
        static struct termios oldt, newt;
        tcgetattr( STDIN_FILENO, &oldt);           // save old settings
        newt = oldt;
        newt.c_lflag &= ~(ICANON);                 // disable buffering      
        tcsetattr( STDIN_FILENO, TCSANOW, &newt);  // apply new settings

        int c = getchar();  // read character (non-blocking)

        tcsetattr( STDIN_FILENO, TCSANOW, &oldt);  // restore old settings
        return c;
}

#define loop_rate_Hz  20

int main(int argc, char **argv)
{
  srand (time(NULL));

  ros::init(argc, argv, "controller");
  ros::NodeHandle nh;
  ros::Rate loop_rate(loop_rate_Hz);
  
  double stepSize = 1.0/20.0 ; 
  double currentTime = 0; 

  bool usingVrep = true;



  AdaptiveController controller(nh);

  cout << "Starting main controller ..." << std::endl;


  while (ros::ok() && !(controller.getSimState() == 22))
  {
    controller.step();

    if(kbhit())
    {
      int key = getch();
      switch(char(key)){
        case 'i':
          controller.increaseFrequency();
          break;
        case 'k':
          controller.decreaseFrequency();
          break;
        default: 
          std::cout << "Not a valid input key" << std::endl;
          break;
      }
    }

    // do{
      ros::spinOnce();
      loop_rate.sleep();
    // }

    // while(ros::ok() && ((controller.getGlobalTime() - currentTime) <= stepSize) && !(controller.getSimState() == 22) && usingVrep);
      // while(ros::ok() && ((controller.getGlobalTime() - currentTime) <= stepSize) && !(controller.getSimState() == 22) && usingVrep){
      //   printf("\n WHILE ");
      // }

    std::cout << "\nCONTROLLER ==> \n";
    std::cout << controller.getGlobalTime() << " - " << currentTime << " || = " << controller.getGlobalTime() - currentTime << " <= " << stepSize << " && ! " << controller.getSimState() << "\n";
    
    if(controller.getGlobalTime() - currentTime <= stepSize ){
      std::cout << "True\n\n"; 
    }
    else{
      std::cout << "False\n\n"; 
    }

    currentTime = controller.getGlobalTime();

    
  } // while 

  cout << "\n\n Stopping main controller ..." << std::endl;
  
  return 0;
}
```


Developed by Thirawat Chuthong (IST, VISTEC)
contact -- thirawat.chu@gmail.com
