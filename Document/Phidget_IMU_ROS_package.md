# Install ROS package for Phidget IMU

## 1. Make a catkin workspace
1. Open terminal
2. As the terminal type
    ```code
    mkdir -p catkin_ws/src
    cd cakin_ws/src
    ```


## 2. Download the metapackage from the github repository
>__<ros_distro>__ may be groovy, hydro, indigo, kinetic
```code
git clone -b <ros_distro> https://github.com/ros-drivers/phidgets_drivers.git
```


## 3. Install dependencies using rosdep
```code
rosdep install phidgets_drivers
```


## 4. Alternatively, if rosdep does not work, install the following packages:
```code
sudo apt-get install libusb-1.0-0 libusb-1.0-0-dev
```


## 5. Compile your catkin_ws
```code
cd ~/catkin_ws && catkin_make && source devel/setup.bash
```
## 6. For RUN this package to get IMU data
```code
roslaunch phidgets_imu imu_single_nodes.launch
```

# Udev rules setup
__Note:__ The following steps are only required when installing the package from source. When installing a binary debian package of phidgets_api >= 0.7.8, the udev rules are set up automatically.

Make sure your catkin workspace has been successfully compiled. To set up the udev rules for the Phidgets USB devices, run the following commands:

```code
roscd phidgets_api
sudo cp debian/udev /etc/udev/rules.d/99-phidgets.rules
sudo udevadm control --reload-rules
```
Afterwards, disconnect the USB cable and plug it in again or run 
```code
sudo udevadm trigger
```


Credit : [Phidgets drivers for ROS](https://github.com/ros-drivers/phidgets_drivers)