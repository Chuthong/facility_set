# Vortex activation

### Request licence from cm-lab first.
Goto this [link](https://www.cm-labs.com/vortex-studio/software/vortex-studio-academic-access/?fbclid=IwAR27iYRutjl-sLOCMf73Rm3fXMfz6jC-Z6GQxejzmsxJYedlzXsHI5b-TeQ)

### Yes!! I got a vortex licence
example licence : 1111-2222-3333-4444

#### 1. Download libraries for linux [here](https://www.cm-labs.com/downloads/?utm_campaign=Vortex%20Studio%20for%20Academics&utm_medium=email&_hsmi=72179436&_hsenc=p2ANqtz--ikHK80Q__9WDBb1sx-_M3JZJjK9jD1Ln6APfeuE2g6p2LwTBSkzLC3AEUJvgJTIs2B7u55f1RB6LwsEIoMGcA3d1ezcfnGQs8AtLObk-MJUxV2SI&utm_content=72179436&utm_source=hs_automation)
** select >> Required Downloads(manual process) >> Vortex Studio(Linux)

#### 2. Extract Downloaded file

#### 3. Goto Extracted folder : "Vortex_Studio_2021.1.0.165_x64_gcc73"
```bash
# goto bin
cd bin
```
#### 4. Check activation and activate licence
Check activation
```bash
./VortexLicenseManager --status
```

Activate licence
```bash
# add key
./VortexLicenseManager --key aaaa-bbbb-cccc-dddd

# activate locally
./VortexLicenseManager --activate-locally aaaa-bbbb-cccc-dddd
```

#### 5. Check by opening CoppeliaSim
- Don't forget to "rosrun"

If vortex does't work
- goto CoppeliaSim folder >> vortexPlugin
- take "libsimVortex.so" out of this folder
- Close and open CoppeliaSim again
____

- [Original document](https://vortexstudio.atlassian.net/wiki/spaces/VSD21B/pages/3229221850/Generating+Node-Locked+License+Files#GeneratingNode-LockedLicenseFiles-GeneratingDongle-LockedLicenseFilesonWindows)
- [More document for vertex](https://vortexstudio.atlassian.net/wiki/spaces/VSD/overview?mode=global)



Developed by Thirawat Chuthong
