# How to Lock USB port
for the hardware integration field. Sometime, you need to connect many USB port to your computer but you don't know which one is, and it will change its name when you unplug and plug in. So why is it?. When port name changed, You need to change your code too, unless you cannot connect to the device you want. This is the way that can help your to lock port name when your unplug and plug it again.  

## 1. Find USB port information
1. Open terminal
2. At terminal type
```code
lsusb
```
This is example you will see.  

![USB port name!](figure/name_USBport.png)

## 2. Make rules file name "USB.rules"
1. At terminal type
    ```code
    cd ~/ && touch USB.rules
    gedit USB.rules
    ```
2. At USB.rules type and see the name and number port at "lsusb"
    ```text
    KERNEL=="ttyUSB*" ,ATTRS{idVendor}=="XXXX" ,ATTRS{idProduct}=="YYYY" ,MODE:="0777" ,SYMLINK="ttyNAME_PORT" 
    ```
    If you have many port you need to do the same with them.

    Example: \
    I have 2 device that I want to lock name
    ```text
    KERNEL=="ttyUSB*" ,ATTRS{idVendor}=="0403" ,ATTRS{idProduct}=="6001" ,MODE:="0777" ,SYMLINK="ttyARDUINO" 

    KERNEL=="ttyUSB*" ,ATTRS{idVendor}=="0403" ,ATTRS{idProduct}=="6014" ,MODE:="0777" ,SYMLINK="ttyU2D2"
    ```

## 3. Add this rules to system
1. Open terminal
2. At terminal type
    ```code
    cd ~/
    sudo cp USB.rules /etc/udev/rules.d/
    sudo udevadm control --reload-rules
    sudo udevadm trigger
    ```



Credit: Arm Sorawis \
Developed by Joe Thirawat Chuthong
